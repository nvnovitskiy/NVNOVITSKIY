<h3 align="center">
  Welcome to Nikita Novitskiy's profile!
  <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="28">
</h3>

<p align="center">
  <img src="https://readme-typing-svg.herokuapp.com?font=Ubuntu&size=25&width=410&lines=Data+Engineer+at+PJSC+Sberbank" alt="https://git.io/typing-svg">
</p>

</p>

## 😎 ABOUT ME

* :herb: At the moment I am studying for a master's degree at the Samara National Research University named after academician S.P. Korolev in the direction of Data Science and work at Sberbank PJSC
* 👨🏻‍💻 I am writing a research paper on the topic: "Development and research of algorithms for building recommendation systems".
* 💬 Ask me anything via e-mail shown in my profile

## 🛠️ MY FAVOURITE TOOLS

### 👨🏻‍💻 PROGRAMMING LANGUAGES

<p>
  <a href="https://isocpp.org/"><img alt="C++" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/c%2B%2B.svg" width="40" height="40"></a>
  <a href="https://www.learn-c.org/"><img alt="C" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/c.svg" width="40" height="40"></a>
  <a href="https://www.javascript.com/"><img alt="JavaScript" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/javascript.svg" width="40" height="40"></a>
  <a href="https://www.python.org/"><img alt="Python" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/python.svg" width="40" height="40"></a>
  <a href="https://www.r-project.org/"><img alt="R" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/r.svg" width="40" height="40"></a>
  <a href="https://www.ruby-lang.org/ru/"><img alt="Ruby" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/ruby.svg" width="40" height="40"></a>
  <a href="https://scala-lang.org/"><img alt="Scala" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/scala.svg" width="40" height="40"></a>

### 🧰 FRAMEWORKS AND LIBRARIES
<p>
  <a href="https://spark.apache.org/"><img alt="Apache Spark" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/spark.svg" width="40" height="40"></a>
  <a href="https://hadoop.apache.org/"><img alt="Apache Hadoop" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/hadoop.svg" width="40" height="40"></a>
    <a href="https://keras.io/"><img alt="Keras" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/keras.svg" width="40" height="40"></a>
  <a href="https://matplotlib.org/"><img alt="Matplotlib" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/matplotlib.svg" width="40" height="40"></a>
  <a href="https://numpy.org/"><img alt="NumPy" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/numpy.svg" width="40" height="40"></a>
  <a href="https://opencv.org/"><img alt="OpenCV" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/opencv.svg" width="40" height="40"></a>
  <a href="https://pandas.pydata.org/"><img alt="Pandas" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/pandas.svg" width="30" height="40"></a>
  <a href="https://pytorch.org/"><img alt="PyTorch" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/torch.svg" width="40" height="40"></a>
  <a href="https://seaborn.pydata.org/"><img alt="Seaborn" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/seaborn.svg" width="40" height="40"></a>
  <a href="https://www.sympy.org/en/index.html"><img alt="SymPy" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/sympy.svg" width="40" height="40"></a>
  <a href="https://www.tensorflow.org/"><img alt="TensorFlow" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/tensorflow.svg" width="40" height="40"></a>

### 🗄️ DATABASES

<p>
    <a href="https://www.mysql.com/"><img alt="MySQL" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/mysql.svg" width="40" height="40"></a>
    <a href="https://www.oracle.com/index.html"><img alt="Oracle" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/oracle.svg" width="40" height="40"></a>
    <a href="https://www.postgresql.org/"><img alt="PostgreSQL" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/postgresql.svg" width="40" height="40"></a>
    <a href="https://www.microsoft.com/ru-ru/sql-server/sql-server-2019"><img alt="SQL Server" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/sql-server.png" width="40" height="40"></a>
</p>

### 💻 SOFTWARE AND TOOLS

<p>
 <a href="https://www.anaconda.com/"><img alt="Anaconda" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/anaconda.svg" width="40" height="40"></a>
   <a href="https://www.jetbrains.com/clion/"><img alt="CLion" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/clion.svg" width="40" height="40"></a>
   <a href="https://git-scm.com/"><img alt="Git" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/git.png" width="40" height="40"></a>
   <a href="https://www.jetbrains.com/idea/"><img alt="Intellij IDEA" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/intellij-idea.svg" width="40" height="40"></a>
   <a href="https://jupyter.org/"><img alt="Jupyter" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/jupyter.svg" width="40" height="40"></a>
   <a href="https://www.jetbrains.com/pycharm/"><img alt="PyCharm" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/pycharm.svg" width="40" height="40"></a>
   <a href="https://www.jetbrains.com/ruby/"><img alt="RubyMine" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/rubymine.svg" width="40" height="40"></a>
   <a href="https://www.jetbrains.com/webstorm/"><img alt="WebStorm" src="https://github.com/nvnovitskiy/nvnovitskiy/blob/main/logos/webstorm.svg" width="40" height="40"></a>
</p>

## 📊 GITHUB STATISTIC

[![Novitskiy's GitHub 1](https://github-readme-streak-stats.herokuapp.com?user=NVNovitskiy&theme=prussian&date_format=M%20j%5B%2C%20Y%5D)](https://github.com/nvnovitskiy)
[![Novitskiy's GitHub 2](https://github-readme-stats.vercel.app/api?username=nvnovitskiy&theme=prussian&show_icons=true)](https://github.com/nvnovitskiy)

